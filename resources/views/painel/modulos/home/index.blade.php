@extends('painel.templates.dashboard')
@section('conteudo')
<div class="title-pg">
    <h1 class="title-pg">Home - Painel de Administração</h1>
</div>

<div class="content-din bg-white">

  
       <!-- Mensagens enviadas do  controller pela session success -->
      @if( Session::has('success'))
                <div class="col-md-12">
                    <div class="alert alert-success alert-dismissible hide-msgd">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <h4><i class="icon fa fa-warning"></i> {{Session::get('success')}}</h4>

                    </div>
                </div>
        @endif
    <!-- /. Mensagens enviadas do  controller pela session success -->
<div class="col-md-3">
<ul class="list-group list-group-flush">
  <li class="list-group-item">Quantidade de Usuarios: {{$total_user}}</li>
  <li class="list-group-item">Posts Ativos: {{$numero_post_ativos}}</li>
  <li class="list-group-item">Posts Rascunho: {{$numero_post_rascunhos}}</li>
  <li class="list-group-item">Posts Destaque: {{$numero_post_destaque}}</li>
  <li class="list-group-item">Quantidade de Categorias: {{$numero_categorias}}</li>
  <li class="list-group-item">Total de posts: {{$total_post}} </li>
</ul>
</div>

</div>
@endsection