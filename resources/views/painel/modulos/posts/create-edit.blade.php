@extends('painel.templates.dashboard') @section('conteudo')
<div class="title-pg">
    <h1 class="title-pg">Cadasro de Post</h1>
</div>

<div class="content-din">

    <!-- Alert Errors start -->
    @if( isset($errors) && count($errors) > 0 )
    <div class="col-md-12">
        <div class="alert alert-warning alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4>
                <i class="icon fa fa-warning"></i> Atenção!</h4>
            @foreach( $errors->all() as $error)
            <p>{{$error}}</p>
            @endforeach
        </div>
    </div>
    @endif
    <!-- /.Alert Errors start -->
    <!-- form start -->
    @if(isset($data))
    <form class="form form-search form-ds" method="post" action="{{route('posts.update', $data->id)}}" enctype="multipart/form-data">
        {{ method_field('PUT') }} @else
        <form class="form form-search form-ds" method="post" action="{{route('posts.store')}}" enctype="multipart/form-data">
            @endif {{ csrf_field() }}

            <input type="hidden" name="user_id" value="{{Auth::User()->id}}">
            <label for="inputCategoria">Selecione a Categoria</label>
            <div class="form-group col-md-12">
                <select name="category_id" class="form-control">
                @if(!isset($data))
                  @foreach( $optionsCreate as $cat)
                   <option value="{{$cat->id}}">{{$cat->name}}</option>
                  @endforeach
                  @else
                  @foreach( $datasInner as $catInner)
                <option value="{{$catInner->category_id}}">{{$catInner->categoria}}</option>
                  @endforeach
                  @foreach( $optionsCreate as $cat)
                   <option value="{{$cat->id}}">{{$cat->name}}</option>
                  @endforeach
                @endif
               
                   
                </select>
            </div>

            <div class="form-group col-md-12">
                <label for="InputName">Titulo</label>
                <input type="text" class="form-control" id="InputName" name="title" placeholder="Título" value="{{$data->title or old('title')}}">
            </div>
       
            <!-- textarea -->
            <div class="form-group col-md-12">
                <label>Descrição</label>
                <textarea class="form-control" rows="5" name="description" placeholder="Digite aqui ...">{{$data->description or old('description')}}</textarea>
            </div>
        
            <div class="form-group col-md-6">
                    <input type="date" name="date" value="{{$data->date or old('date')}}">
                    </br>
                    <input type="text"  name="hour" placeholder="00:00:00" value="{{$data->hour or old('hour')}}" >
                </div>
            
                 
                <div class="form-group col-md-6">
               <label>Destaque?</label>
                <select name="featured" class="form-control">
                @if(!isset($data))
                <option value="0">0 - Sem Destaque</option>
                <option value="1">1 - Com Destaque</option>
                @else
                @if ($data->featured == 0)
                <option value="{{$data->featured}}">0 - Sem Destaque</option>
                <option value="0">0 - Sem Destaque</option>
                <option value="1">1 - Com Destaque</option>
                 @else
                 <option value="{{$data->featured}}">1 - Com Destaque</option>
                <option value="0">0 - Sem Destaque</option>
                <option value="1">1 - Com Destaque</option>
                 @endif

                @endif
                
                    
                </select>
            </div>
                             
               <div class="form-group col-md-12">
               <label>Ativo ou Rascunho?</label>
               <select name="status" class="form-control">
               @if(!isset($data))
               <option value="A">A - Ativo Postado</option>
                    <option value="R">R - Rascunho, não postado</option>
                @else
                <option value="{{$data->status}}">{{$data->status}}</option>
                <option value="A">A - Ativo Postado</option>
                    <option value="R">R - Rascunho, não postado</option>
                @endif
                   
                </select>
            </div>

            <div class="form-group col-md-6">
                <button class="btn btn-info">Enviar</button>
            </div>
        </form>

</div>
<!--Content Dinâmico-->
@endsection