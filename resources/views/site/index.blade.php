@extends('site.templates.master')
@section('conteudo')

<div class="slide">
    <div class="col-md-8">
        <article class="img-big">
            <a href="" title="">
                <img src="imgs/img1.jpg" alt="" class="img-slide-big">

                <h1 class="text-slide">
                    Uma nova maneira de trabalhar com HTML5 - Acesse o Curso HTML5
                </h1>
            </a>
        </article>
    </div>

    <div class="col-md-4">
        <article class="img-small col-md-12 col-sm-6 col-xm-12">
            <a href="" title="">
                <img src="imgs/img2.jpg" alt="" class="img-slide-small">

                <h1 class="text-slide">
                    Ciência de Dado é o novo futuro
                </h1>
            </a>
        </article>
        <article class="img-small col-md-12 col-sm-6 col-xm-12">
            <a href="" title="">
                <img src="imgs/img3.jpg" alt="" class="img-slide-small">
                <h1 class="text-slide">
                    Desvendando Big Data
                </h1>
            </a>
        </article>
    </div>
</div><!--Slide-->


<section class="content">
    <div class="col-md-8">
        
       @foreach ($datasPost as $post)
        <article class="post">
            <div class="image-post col-md-4 text-center">
                <img src="imgs/img1.jpg" alt="Nome Post" class="img-post">
            </div>
            <div class="description-post col-md-8">
                <h2 class="title-post">{{$post['title']}}</h2>

                <p class="description-post">
                   {{$post->description}}
                </p>

                <a class="btn-post" href="?pg=post">Ir <span class="glyphicon glyphicon-chevron-right"></span></a>
            </div>
        </article>
       @endforeach

        <div class="pagination-posts">
            <nav aria-label="Page navigation">
              <ul class="pagination">
                              
                {{-- {{$datasPost->links()}} --}}

    @if(isset($dataForm))
    {{$datasPost->appends(Request::only('pesquisa'))->links()}}
        @else
    {{$datasPost->links()}}
        @endif
           
                   
              </ul>
            </nav>
        </div>

    </div><!--POSTS-->

    <!--Sidebar-->
    <div class="col-md-4">
    <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FfaculdadeAlfaUmuarama%2F&tabs=timeline&width=340&height=500&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=316115088513380" width="340" height="500" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>		</section>

@endsection
       