<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'posts';

    protected $fillable = [
        'user_id', 'category_id', 'title','description','date','hour','featured','status'
    ];

    

    public function categories(){
        return $this->belongsTo('App\Models\Category','id');

    }
    
    public function rules(){
        return [
            'title'        => 'required|min:3|max:100',
            'description'         => 'required|min:3|max:1000'
            
        ];
    }
    
    
}
