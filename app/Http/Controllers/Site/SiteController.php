<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Models\Category;
use App\Models\Post;

class SiteController extends Controller
{
    public function __construct(Category $category, Post $post){
        $this->modelCategory = $category;
        $this->modelPost = $post;
        $this->categories = Category::all();
        $this->post = Post::where('posts.featured','1');
       
    }

    public function index()
    {
        $datas = $this->categories;
        $datasPost = $this->post->paginate(10);
    
        return view ('site.index', compact('datas','datasPost'));
    }

    public function categoria($id)
    {
       $datas = $this->categories;
       $datasPost = $this->modelPost::where('category_id',$id)->paginate(5);
       $datasCategorie = $this->modelCategory->find($id);
       return view ('site.pages.categoria', compact('datas','datasCategorie','datasPost'));
       // dd($datasPost );
    }

    public function post($id)
    {
        $datas = $this->categories;

        //$datasPost = $this->post->where('posts.id',$id)->paginate(10);
        $datasPost = $this->post->join('users', 'users.id', '=', 'posts.user_id')
                            ->where('posts.id',$id)
                            ->select('users.name','posts.*')
                            ->get();
          $postMin = $id - 2;
          $postMax = $id + 2;                  

         $postsRelacionados = $this->modelPost
         ->where('posts.status', '=', 'A')
         ->where('posts.id', '>', $postMin)
         ->where('posts.id', '<', $postMax)
          ->get();

        return view ('site.pages.post', compact('datas','datasPost','postsRelacionados'));
        //dd($postsRelacionados);
    }

    public function empresa()
     {
      $datas = $this->categories;

        return view ('site.pages.empresa',compact('datas'));
    }

    public function contato()
    {
        $datas = $this->categories;
        return view ('site.pages.contato', compact('datas'));
    }
}
