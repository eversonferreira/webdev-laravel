<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Controllers\Painel\StandardController;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Models\Post;
use App\Models\Category;
use App\User;
use DB;

class PostController extends StandardController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $model;
    protected $modelAuxiliar;
    protected $modelInner;
    protected $modelid = 'posts.id';
    protected $modelname = 'posts.title';
    protected $totalpages = 10;
    protected $views = 'painel.modulos.posts';
    protected $rotas = 'posts';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   
 public function __construct(Post $post, Category $categoria, User $user){
        
 $EloqInner =  DB::table('posts')
        ->join('categories', 'posts.category_id', '=', 'categories.id')
        ->join('users', 'posts.user_id', '=', 'users.id')
        ->select('posts.*','categories.name as categoria', 'users.name as created_user')
        ->orderBy('posts.created_at', 'desc')
        ->orderBy('posts.featured', 'desc')
        ->orderBy('posts.title', 'asc');
        //->paginate(10);
        //->get();
        
        //$post = new App\Models\Post;
        
       $this->modelInner = $EloqInner; 
       $this->model = $post;
       $this->modelAuxiliar = $categoria;
    }
}
