<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Controllers\Painel\StandardController;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Models\Category;
use DB;

class CategoriaController extends StandardController
{

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $model;
    protected $modelAuxiliar;
    protected $modelInner;
    protected $modelid = 'categories.id';
    protected $modelname = 'categories.name';
    protected $totalpages = 10;
    protected $views = 'painel.modulos.categorias';
    protected $rotas = 'categorias';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(Category $category)
    {
        $EloqInner =  DB::table('categories');

        $this->modelInner = $EloqInner; 
        $this->model = $category;
        $this->modelAuxiliar = $category;
    }

   
}
