<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Models\Post;
use App\Models\Category;
use App\User;
use DB;

class HomeController extends Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $model;
    protected $modelAuxiliar;
    protected $modelInner;
    protected $modelid = 'categories.id';
    protected $modelname = 'categories.name';
    protected $totalpages = 10;
    protected $views = 'painel.modulos.home';
    protected $rotas = 'home';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(Post $post, Category $category, User $user)
    {
        $EloqInner =  DB::table('posts')
          ->selectRaw('count(posts.id)  as total_posts' );
        //->get();
        $this->numeroDeUsers = $user::all()->count();
        $this->numeroDeCategorias = $category::all()->count();
        $this->numeroDePost = $post::all()->count();
        $this->numeroDePostAtivos = $post::where('status', 'A')->count();
        $this->numeroDePostRascunhos = $post::where('status', 'R')->count();
        $this->numeroDePostDestaque = $post::where('status', 'A')->where('featured', '1')->count();
        $us = $user::all();
       
        //$array = [$cat, $pos,3];
        

      // $this->numeroDePost = $numeroDePost; 
      //  $this->model = $category;
       // $this->modelAuxiliar = $category;
    }
        public function index()
        {
           $total_post = $this->numeroDePost;
           $total_user = $this->numeroDeUsers;
           $numero_categorias = $this->numeroDeCategorias;
           $numero_post_ativos = $this->numeroDePostAtivos;
           $numero_post_rascunhos = $this->numeroDePostRascunhos;
           $numero_post_destaque = $this->numeroDePostDestaque;

          //  $datasInner = $this->modelInner->paginate($this->totalpages);
          
          return view ("{$this->views}.index", compact('total_user','total_post','numero_post_ativos','numero_post_rascunhos','numero_post_destaque','numero_categorias'));
          //dd($datas);
    //echo "oi";
           
    
          }
    
   
}

